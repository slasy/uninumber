using System;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UniNumber;

namespace number_tests
{
    [TestClass]
    public class NumberTest
    {
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestMethod]
        public void AutomaticConversions()
        {
            Number number = byte.MaxValue;
            Assert.AreEqual<byte>(byte.MaxValue, (byte)number);
            number = short.MaxValue;
            Assert.AreEqual<short>(short.MaxValue, (short)number);
            number = int.MaxValue;
            Assert.AreEqual<int>(int.MaxValue, (int)number);
            number = long.MaxValue;
            Assert.AreEqual<long>(long.MaxValue, (long)number);
            number = float.MaxValue;
            Assert.AreEqual<float>(float.MaxValue, (float)number);
            number = double.MaxValue;
            Assert.AreEqual<double>(double.MaxValue, (double)number);
            number = decimal.MaxValue;
            Assert.AreEqual<decimal>(decimal.MaxValue, (decimal)number);
        }

        [TestMethod]
        public void ImplicitConversions()
        {
            Number number = (byte)10;
            number += 1;
            Assert.AreEqual<byte>(11, number);
            number += (short)20;
            Assert.AreEqual<byte>(31, number);
            number += 41;
            Assert.AreEqual<int>(72, number);
            number += 1.2F;
            Assert.That.AreEqualNumbers(73.2f, number);
            number += 2.4D;
            Assert.That.AreEqualNumbers(75.6D, number);
            number += 5.44M;
            Assert.That.AreEqualNumbers(81.04M, number);

            Assert.AreEqual<byte>(81, number);
            Assert.AreEqual<short>(81, number);
            Assert.AreEqual<int>(81, number);
            Assert.AreEqual<long>(81, number);
            Assert.That.AreEqualNumbers(81.04F, number);
            Assert.That.AreEqualNumbers(81.04D, number);
        }

        [DataTestMethod]
        [DataRow(0D, 0D)]
        [DataRow(1D, 1D)]
        [DataRow(-1D, 1D)]
        [DataRow(-1D, -1D)]
        [DataRow(1D, -1D)]
        [DataRow(-0.1D, 0.1D)]
        [DataRow(-1.1D, 1.1D)]
        [DataRow(-10.1D, 10.1D)]
        [DataRow(-1000.1D, 1000.1D)]
        [DataRow(300D, 600D)]
        [DataRow(20_000D, 99_000D)]
        [DataRow(20_000D, -33_333D)]
        public void Calculations(double a_value, double b_value)
        {
            List<Func<dynamic, dynamic, dynamic>> f = new List<Func<dynamic, dynamic, dynamic>>{
                Add, Sub, Mul, Div, Mod, And, Or, Xor
            };
            foreach (var function in f)
            {
                if ((function == Div || function == Mod) && (byte)b_value == 0) continue;
                Number a = (byte)a_value;
                Number b = (byte)b_value;
                Number c = (byte)function(a, b);
                Assert.AreEqual<byte>((byte)function((byte)a_value, (byte)b_value), c);
                a = (long)a_value;
                b = (long)b_value;
                c = (long)function(a, b);
                Assert.AreEqual<long>((long)function((long)a_value, (long)b_value), c);
                if (function == And || function == Or || function == Xor) continue;
                a = (float)a_value;
                b = (float)b_value;
                c = (float)function(a, b);
                Assert.AreEqual<float>((float)function((float)a_value, (float)b_value), c);
                a = (float)a_value;
                b = (double)b_value;
                c = (double)function(a, b);
                Assert.AreEqual<double>((double)function((float)a_value, (double)b_value), c);
                a = (decimal)a_value;
                b = (decimal)b_value;
                c = (decimal)function(a, b);
                Assert.AreEqual<decimal>((decimal)function((decimal)a_value, (decimal)b_value), c);
                a = (decimal)a_value;
                b = (byte)b_value;
                c = (decimal)function(a, b);
                Assert.AreEqual<decimal>((decimal)function((decimal)a_value, (byte)b_value), c);
            }
        }

        [DataTestMethod]
        [DataRow(0, 0)]
        [DataRow(0, 1)]
        [DataRow(1, 0)]
        [DataRow(1, 1)]
        [DataRow(-1, 0)]
        [DataRow(0, -1)]
        [DataRow(-1, -1)]
        public void Compare(int a_value, int b_value)
        {
            Number a = a_value;
            Number b = b_value;
            Assert.AreEqual(a_value == b_value, a == b);
            Assert.AreEqual(a_value != b_value, a != b);
            Assert.AreEqual(a_value > b_value, a > b);
            Assert.AreEqual(a_value < b_value, a < b);
            Assert.AreEqual(a_value >= b_value, a >= b);
            Assert.AreEqual(a_value <= b_value, a <= b);
        }

        [TestMethod]
        public void Benchmark()
        {
            const int repeats = 2_000_000;
            var functions = new List<Func<int, int, int>> { AddInteger, DivInteger }
                .Zip(new List<Func<Number, Number, Number>> { AddNumber, DivNumber }, (a, b) => Tuple.Create(a, b))
                .ToList();
            foreach (var functionTuple in functions)
            {

                Stopwatch time = new Stopwatch();
                time.Start();
                for (int i = 0; i < repeats; i++)
                {
                    int result = functionTuple.Item1(10, 20);
                    result = functionTuple.Item1(30, 40);
                    result = functionTuple.Item1(50, -50);
                }
                time.Stop();
                long normal = time.ElapsedMilliseconds;
                TestContext.WriteLine($"normal - calculation: {normal}ms");
                time.Restart();
                for (int i = 0; i < repeats; i++)
                {
                    Number result = functionTuple.Item2(10, 20);
                    result = functionTuple.Item2(30, 40);
                    result = functionTuple.Item2(50, -50);
                }
                time.Stop();
                TestContext.WriteLine($"Number<integer> - calculation: {time.ElapsedMilliseconds}ms");
                time.Restart();
                for (int i = 0; i < repeats; i++)
                {
                    int result = functionTuple.Item2(10, 20);
                    result = functionTuple.Item2(30, 40);
                    result = functionTuple.Item2(50, -50);
                }
                time.Stop();
                TestContext.WriteLine($"Number<integer> -> int - calculation: {time.ElapsedMilliseconds}ms");
                TestContext.WriteLine($"Number is {time.ElapsedMilliseconds / (float)normal:0.00}x slower");
            }
        }

        [TestMethod]
        public void UnaryOperators()
        {
            double test_value = -0.0005D;
            Number n = test_value;
            n = +n;
            test_value = +test_value;
            Assert.AreEqual<double>(test_value, n);
            n = -n;
            test_value = -test_value;
            Assert.AreEqual<double>(test_value, n);
            n++;
            test_value++;
            Assert.AreEqual<double>(test_value, n);
            n--;
            test_value--;
            Assert.AreEqual<double>(test_value, n);
            byte b_test_value = 0x0F;
            n = b_test_value;
            n = ~n;
            Assert.AreEqual<byte>((byte)(~b_test_value), n);
        }

        private int AddInteger(int a, int b) => a + b;
        private Number AddNumber(Number a, Number b) => a + b;
        private int DivInteger(int a, int b) => a / b;
        private Number DivNumber(Number a, Number b) => a / b;

        private dynamic Add(dynamic a, dynamic b) => a + b;
        private dynamic Sub(dynamic a, dynamic b) => a - b;
        private dynamic Mul(dynamic a, dynamic b) => a * b;
        private dynamic Div(dynamic a, dynamic b) => a / b;
        private dynamic Mod(dynamic a, dynamic b) => a % b;
        private dynamic And(dynamic a, dynamic b) => a & b;
        private dynamic Or(dynamic a, dynamic b) => a | b;
        private dynamic Xor(dynamic a, dynamic b) => a ^ b;
    }

    static class AssertExt
    {
        public static void AreEqualNumbers(this Assert t, float expected, float actual, float e = 0.00001F)
        {
            if (Math.Abs(expected - actual) > e)
            {
                throw new AssertFailedException($"Expected: <{expected}+-{e}>. Actual: <{actual}>");
            }
        }

        public static void AreEqualNumbers(this Assert t, double expected, double actual, double e = 0.00001D)
        {
            if (Math.Abs(expected - actual) > e)
            {
                throw new AssertFailedException($"Expected: <{expected}+-{e}>. Actual: <{actual}>");
            }
        }

        public static void AreEqualNumbers(this Assert t, decimal expected, decimal actual, decimal e = 0.00001M)
        {
            if (Math.Abs(expected - actual) > e)
            {
                throw new AssertFailedException($"Expected: <{expected}+-{e}>. Actual: <{actual}>");
            }
        }
    }
}
