<Query Kind="Program">
  <NuGetReference>Rock.Core.Newtonsoft</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Converters</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Runtime.InteropServices</Namespace>
</Query>

void Main()
{
	Number n = 1;		// 1
	Console.WriteLine($"{n}");
	n = n + 1;			// 2
	Console.WriteLine($"{n}");
	n = n - 0.1f;		// 1.9
	Console.WriteLine($"{n}");
	n = n - 0.2d;		// 1.7
	Console.WriteLine($"{n}");
	n = n + 3L;			// (1.7D => 1L) + 3 = 4
	Console.WriteLine($"{n}");
	long f = n;			// 4
	Console.WriteLine($"{f}");
}

[StructLayout(LayoutKind.Explicit)]
public struct Number
{
	private enum NumberType : byte
	{
		INT, LONG, FLOAT, DOUBLE,
	}
	[FieldOffset(0)] private float float_;
	[FieldOffset(0)] private int int_;
	[FieldOffset(0)] private long long_;
	[FieldOffset(0)] private double double_;
	[FieldOffset(8)] private NumberType type;
	
	public static implicit operator Number(float value) => new Number { float_ = value, type = NumberType.FLOAT };
	public static implicit operator Number(int value) => new Number { int_ = value, type = NumberType.INT };
	public static implicit operator Number(long value) => new Number { long_ = value, type = NumberType.LONG };
	public static implicit operator Number(double value) => new Number { double_ = value, type = NumberType.DOUBLE };
	
	public static implicit operator float(Number number) => number.GetFloat();
	public static implicit operator int(Number number) => number.GetInt();
	public static implicit operator long(Number number) => number.GetLong();
	public static implicit operator double(Number number) => number.GetDouble();
	
	public override string ToString()
	{
		return $"Number<type {type}; int({int_}); long({long_}); float({float_}); double({double_})>";
	}
	
	private int GetInt()
	{
		switch (type)
		{
			case NumberType.INT: return int_;
			case NumberType.LONG: return (int)long_;
			case NumberType.FLOAT: return (int)float_;
			case NumberType.DOUBLE: return (int)double_;
			default: throw new Exception();
		}
	}
	
	private long GetLong()
	{
		switch (type)
		{
			case NumberType.INT: return (long)int_;
			case NumberType.LONG: return long_;
			case NumberType.FLOAT: return (long)float_;
			case NumberType.DOUBLE: return (long)double_;
			default: throw new Exception();
		}
	}
	
	private float GetFloat()
	{
		switch (type)
		{
			case NumberType.INT: return (float)int_;
			case NumberType.LONG: return (float)long_;
			case NumberType.FLOAT: return float_;
			case NumberType.DOUBLE: return (float)double_;
			default: throw new Exception();
		}
	}
	
	private double GetDouble()
	{
		switch (type)
		{
			case NumberType.INT: return (double)int_;
			case NumberType.LONG: return (double)long_;
			case NumberType.FLOAT: return (double)float_;
			case NumberType.DOUBLE: return double_;
			default: throw new Exception();
		}
	}
}